# SilverStripe Snippets
## Jeremy Roundill

### Modifying CMS fields

>     public function getCMSFields() {
>         $fields = parent::getCMSFields();
>         $fields->addFieldToTab('Root.Main', new TextField('Name'));
>         
>         return $fields;
>     }

### Handling staged objects

>     
>     Versioned::reading_stage('Stage');
>     YourDataObject::get();
>

### Setting Image Quality

Default image quality is set at 75, update it in a config.yml file

>
>     GDBackend:
>       default_quality: 90
>

### Template loop variables

>
>     <% loop $List %>
>         $Pos - (Int) Position (starting at 1) <br/>
>         $Modulus(5) - (Int) $Pos mod 5 <br/>
>         $MultipleOf(4) - (Bool) $Pos is a multiple of 4 <br/>
>         $Even - (Bool) $Pos is even <br/>
>         $Odd - (Bool) $Pos is odd <br/>
>         $EvenOdd - (String) "even" or "odd"
>         $TotalItems - (Int) $Number of items in the list <br/>
>         $First - (Bool) Is first position in list <br/>
>         $Middle - (Bool) Is not first or last position in list <br/>
>         $Last - (Bool) Is last position in list <br/>
>     <% end_loop %>
>

### Dropdownfield placeholder

>     DropdownField::create("Name", "Display Name")->setHasEmptyDefault(true)

### Example Buildtask

This will be listed under **Your Build Task** in */dev/tasks* and can be run using */dev/tasks/YourBuildTask*

>     class YourBuildTask extends BuildTask {
>         protected $title = 'Your Build Task';
>         protected $description = 'This is a description!';
>         protected $enabled = true;
>     
>         function run($request) {
>             // Do the things here
>         }
>     }

### Routing to a controller

Route in *config.yml*

>     Director:
>         rules: 
>             'example/route' : 'Example_Controller'

**Example_Controller** in *mysite/code/Example.php*

>     class ExamplePage_Controller extends Page_Controller
>     {
>         private static $allowed_actions = array(
>             'index'
>         ); 
>         public function index() {
>             return $this->renderWIth(
>                 array(
>                     'ExamplePage', 
>                     'Page'
>                 )
>             );
>         }
>     }

This will render with *themes/**my_theme**/Layout/ExamplePage.ss*

You can see the result at **mysite.com**/example/route - Remember to flush!

### Setting the default login destination

yaml: 

>     Security:
>       default_login_dest: '/yourdestination'

php:

>     Security::set_default_login_dest('yourdestination');

### Getting get variables (or params)

In a controller:

>     $this->getRequest()->getVar('yourvar');

Outside of a controller:

>     Controller::curr()->getRequest()->getVar('yourvar');

Just swap out **getVar** for **param** if you want to get a variable from a route you've defined

### Creating a GridField

>     $fields->addFieldToTab('Root.Board', new GridField("BoardMembers", "Board Members", $this->BoardMembers(), GridFieldConfig_RecordEditor::create()));

Swap out `GridFieldConfig_RecordEditor` for a selection of

 - `GridFieldConfig_Base`
 - `GridFieldConfig_RecordViewer`
 - `GridFieldConfig_RecordEditor`
 - `GridFieldConfig_RelationEditor`

### Changing CSV export fields on a GridField

>     function getCMSFields() {
>         $fields = parent::getCMSFields();
> 
>         $gridField = $fields->fieldByName('Report');
>         $gridFieldConfig = $gridField->getConfig();
>         $export = $gridFieldConfig->getComponentByType('GridFieldExportButton');
> 
>         $export->setExportColumns(array(
>             // Put fields here
>         ));
> 
>         return $fields;
>     }

### Editing SiteConfig

Creating a custom SiteConfig

>     <?php
>     class CustomSiteConfig extends DataExtension {
>     
>         private static $db = array(
>             'ContactEmail' => 'HTMLText'
>         );
>     
>         public function updateCMSFields(FieldList $fields) {
>             $fields->addFieldToTab("Root.Main", 
>                 new TextField("ContactEmail")
>             );
>         }
>     }

Enabling the custom SiteConfig

>     SiteConfig:
>       extensions:
>         - CustomSiteConfig

Accessing the new field in a template

>     $SiteConfig.ContactEmail 

Accessing the new field in a controller

>     $site_config = $this->SiteConfig();
>     echo $site_config->ContactEmail;

Accessing the new field outside of a controller

>     $site_config = SiteConfig::current_site_config();
>     echo $site_config->ContactEmail;

### Adding edit functionality to row in report

>     function getCMSFields() {
>         $fields = parent::getCMSFields();
>         
>         $gridField = $fields->fieldByName('Report');
>         $gridFieldConfig = $gridField->getConfig();
>         $gridFieldConfig->addComponent(new GridFieldEditButton());
>         $gridFieldConfig->addComponent(new GridFieldDetailForm());
>         
>         return $fields;
>     }

### Example _ss_environment file

>     <?php
>     /* What kind of environment is this: development, test, or live (ie, production)? */
>     define('SS_ENVIRONMENT_TYPE', 'dev');
>     
>     /* Database connection */
>     define('SS_DATABASE_SERVER', 'localhost');
>     define('SS_DATABASE_NAME', 'db_name');
>     define('SS_DATABASE_USERNAME', 'db_user');
>     define('SS_DATABASE_PASSWORD', 'db_pass');

Remove $databaseConfig from _config.php, and add this line

>     require_once("conf/ConfigureFromEnv.php");

### Remove field from tab

>     $fields->removeFieldFromTab('Root.Main', 'DisplayPrice');

### Example ModelAdmin

>     class PersonAdmin extends ModelAdmin {
>         private static $url_segment = 'people';
>         private static $menu_title = 'Person Admin';
>         private static $managed_models = array('Person');
>     }